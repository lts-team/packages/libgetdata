.\" gd_getdata.3.  The gd_getdata man page.
.\"
.\" Copyright (C) 2011, 2012, 2013, 2014, 2015 D. V. Wiebe
.\"
.\""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
.\"
.\" This file is part of the GetData project.
.\"
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Sections, with no Front-Cover Texts, and with no Back-Cover
.\" Texts.  A copy of the license is included in the `COPYING.DOC' file
.\" as part of this distribution.
.\"
.TH gd_seek 3 "25 September 2015" "Version 0.9.0" "GETDATA"
.SH NAME
gd_seek \(em reposition a dirfile field pointer
.SH SYNOPSIS
.B #include <getdata.h>
.HP
.nh
.ad l
.BI "off_t gd_seek(DIRFILE *" dirfile ", const char *" field_code ", off_t"
.IB frame_num ", off_t " sample_num ", int " flags );
.hy
.ad n
.SH DESCRIPTION
The
.BR gd_seek ()
function changes the position of the I/O pointer associated with the field
.I field_code
in the dirfile(5) database specified by
.IR dirfile .
In normal operation,
.BR gd_seek ()
advances the field I/O pointer
.I frame_num
frames plus
.I sample_num
samples from the origin point specified in
.IR flags ,
which should contain one of
.BR GD_SEEK_SET ", " GD_SEEK_CUR ,
or
.BR GD_SEEK_END ,
indicating, respectively, sample zero, the current position of the
field pointer, and the location of the end-of-field marker (see
.BR gd_eof (3)).

In addition to one of the symbols above, the
.I flags
parameter may also, optionally, be bitwise or'd with
.BR GD_SEEK_WRITE ,
which will result in the field being padded (with zero for integer types, or a
IEEE-754 conforming not-a-number otherwise) in the event of seeking past the
end-of-field marker.

The effect of attempting to seek past the end-of-field is encoding specific.
Some encodings don't actually add the padding requested by
.B GD_SEEK_WRITE
unless a subsequent
.BR gd_putdata (3)
call is used to add more data to the field at the new end-of-field.  Other
encodings add the padding, advancing the end-of-field, regardless of subsequent
writes.  Similarly, attempting to seek past the end-of-field marker in read mode
(without specifying
.BR GD_SEEK_WRITE )
is also encoding specific: in some encodings the field pointer will be moved
past the end-of-field marker, while in others, it will be repositioned to the
end of field.  Check the return value to determine the result.

In general,
.B GD_SEEK_WRITE
should be used on
.BR gd_seek ()
calls before a write via
.BR gd_putdata (3),
while calls before a read via
.BR gd_getdata (3)
should omit the
.B GD_SEEK_WRITE
flag.  So the following:
.IP
.nh
.ad l
.BI "gd_seek(" dirfile ", " field_code ", " a ", " b ,
.B GD_SEEK_SET | GD_SEEK_WRITE);
.br
.BI "gd_putdata(" dirfile ", "field_code ", GD_HERE, 0, " c ", " d ", " type ,
.IB data );
.ad n
.hy
.P
is equivalent to:
.IP
.nh
.ad l
.BI "gd_putdata(" dirfile ", "field_code ", " a ", " b ", " c ", " d ", " type ,
.IB data );
.P
and, similarly,
.IP
.nh
.ad l
.BI "gd_seek(" dirfile ", " field_code ", " a ", " b ", GD_SEEK_SET);"
.br
.BI "gd_getdata(" dirfile ", "field_code ", GD_HERE, 0, " c ", " d ", " type ,
.IB data );
.ad n
.hy
.P
is equivalent to:
.IP
.nh
.ad l
.BI "gd_getdata(" dirfile ", "field_code ", " a ", " b ", " c ", " d ", " type ,
.IB data );
.P
Only
.B RAW
fields (and the implicit
.I INDEX
field) have field I/O pointers associated with them.  Calling
.BR gd_seek ()
on a derived field will move the field pointers of all of the field's inputs.
It is possible to create derived fields which simultaneously read from different
places of the same input field.  Calling
.BR gd_seek ()
on such a field will result in an error.

.SH RETURN VALUE
Upon successful completion,
.BR gd_seek ()
returns the field position of the specified field in samples.  On error, it
returns -1 and set the dirfile error to a non-zero value.  Possible error values
are:
.TP 8
.B GD_E_ALLOC
The library was unable to allocate memory.
.TP
.B GD_E_ARGUMENT
The
.I flags
parameter didn't contain exactly one of
.BR GD_SEEK_SET ", " GD_SEEK_CUR ,
or
.BR GD_SEEK_END .
.TP
.B GD_E_BAD_CODE
The field specified by
.IR field_code ,
or one of the fields it uses for input, was not found in the database.
.TP
.B GD_E_BAD_DIRFILE
The supplied dirfile was invalid.
.TP
.B GD_E_BAD_FIELD_TYPE
An attempt was made to seek relative to
.B GD_SEEK_END
on the
.I INDEX
field, which has no end-of-field marker.
.TP
.B GD_E_DIMENSION
The specified field or one of its inputs wasn't of vector type.
.TP
.B GD_E_DOMAIN
The field position couldn't be set due to a derived field reading simultaneously
from more than one place in a
.B RAW
field.
.TP
.B GD_E_INTERNAL_ERROR
An internal error occurred in the library while trying to perform the task.
This indicates a bug in the library.  Please report the incident to the
maintainer.
.TP
.B GD_E_IO
An error occurred while trying to open or read from a file on disk containing
a raw field.
.TP
.B GD_E_RANGE
The request resulted an attempt to move the I/O pointer of the specified field
or one of its inputs to a negative sample number.
.TP
.B GD_E_RECURSE_LEVEL
Too many levels of recursion were encountered while trying to resolve
.IR field_code .
This usually indicates a circular dependency in field specification in the
dirfile.
.TP
.B GD_E_UNKNOWN_ENCODING
The encoding scheme of a RAW field could not be determined.  This may also
indicate that the binary file associated with the RAW field could not be found.
.TP
.B GD_E_UNSUPPORTED
Reading from dirfiles with the encoding scheme of the specified dirfile is not
supported by the library.  See
.BR dirfile-encoding (5)
for details on dirfile encoding schemes.
.PP
The dirfile error may be retrieved by calling
.BR gd_error (3).
A descriptive error string for the last error encountered can be obtained from
a call to
.BR gd_error_string (3).
.SH SEE ALSO
.BR gd_open (3),
.BR gd_getdata (3),
.BR gd_putdata (3),
.BR gd_tell (3).
